#ifndef SUPPORTED_FORMATS_H
#define SUPPORTED_FORMATS_H

#include <QStringList>
//Function defines supported formats
inline void fillWithSupportedFormats(QStringList &list)
{
    list << "*.mp3" << "*.flac";
}

#endif
