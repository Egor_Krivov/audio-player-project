#ifndef OUR_PATHS_AND_CONSTANTS_H
#define OUR_PATHS_AND_CONSTANTS_H

#define BUF 256
#define PROJ 22
#define ISDEBUG 2 //0 - everything in console; 4 nothing in console
//IPC
const char keyPath[BUF] = "../data/key";
//Databases
const char tableName[BUF] = "media_library";
const char databasePath[BUF] = "../data/db.sqlite";
const char mediaLibraryPathsFile[BUF] = "../data/media_lib_paths";
const char tempDatabasePath[BUF] = "../data/temp_db.sqlite";

const char connectionMain[BUF] = "main";
const char connectionTemp[BUF] = "temp";
//Execution
const char masterPath[BUF] = "../lab/master";//тут хранятся бинарные файлы
const char daemonPath[BUF] = "../lab/daemon";

//Playlists
const char playlistDir[BUF] = "../data/playlists";

//logfile
const char logfilepath[BUF] = "../data/log";
//const char configPath[BUF] = "/etc/project/";//тут хранятся статичные файлы, которые почти никогда не меняются
//const char dataBasePath[BUF] = "/var/project/";//тут хранятся динамично изменяющиеся файлы (например, база данных)

#endif // OUR_PATHS_H


