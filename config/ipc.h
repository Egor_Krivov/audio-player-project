#ifndef AEA_IPC_H
#define AEA_IPC_H

#include <stdint.h>
#include <QString>

#define MSGSZ 0x1000
#define LIST_SIZE 20


struct Message
{
	long mtype;
    struct
    {
        int32_t command;
        char buf[MSGSZ];
    } msg;
};
enum message_types {COMMAND_FROM_MASTER_TO_DAEMON = 1,
                COMMAND_FROM_DAEMON_TO_MASTER = 2,
                REPLY_FROM_MASTER_TO_DAEMON = 3};
enum base_commands {PLAY = 0x10, ADD = 0x20, DELETE = 0x30, NEXT = 0x40, PREV = 0x50,
                      PAUSE = 0x60, CREATE = 0x70, LOAD = 0x80, SAVE = 0x90, LIST = 0xA0,
                      CHMODE = 0xB0, VOL = 0xC0, EXIT = 0xD0, REW = 0xE0, STOP = 0xF0,
                        UPDATE = 0x100,
                    MASK_FOR_BASE_COMMAND = 0xFF0};
enum spec_commands {NO_DATA = 0x0, DATA = 0x1, MEDIA = 0x2, PLAYLIST = 0x4,
                    MASK_FOR_SPECIFICATION = 0xF};

enum daemon_commands {OVER = 0x0, PRINT = 0x1, GET_ANSWER = 0x2};

int recogniseBaseCommand (QString command); //It's part of master. Why is it here?
int recogniseSpecCommand (QString command);
int sendMessage (int key, int com, QString bufMes, int msgType); //If cBufMes == NULL then we don't need to fill message.msg
int receiveMessage (int key, Message *message, int msgType);
void deleteMessageQueue (int key);

#endif
