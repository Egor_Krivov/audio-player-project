#include <iostream>
#include <sys/msg.h>
#include "../config/ipc.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <QDebug>

extern int errno;

int sendMessage (int key, int com, QString bufMes, int msgType)
{
    int msgID = 0, msgFlg = 0;
    //Magic number 0666
    msgFlg = IPC_CREAT | 0666;
    Message message;
    memset(&message, 0, sizeof(Message));
//..Opening or creating message queue
    if ((msgID = msgget(key, msgFlg)) < 0)
    {
        qFatal("The message queue wasn't created: %s", strerror(errno));
    }
//..Saving data to struct message
    message.mtype = msgType;
    message.msg.command = com;
    strncpy(message.msg.buf, bufMes.toLocal8Bit().constData(), bufMes.length());
//..Send message
    msgFlg = 0;
   if (msgsnd(msgID, &message, sizeof(message.msg), msgFlg) < 0) {
       qDebug() << strerror(errno);
       qFatal("Couldn't send a message");
    }
    qDebug() << "message send";
    return 0;
}

int receiveMessage (int key, Message *message, int msgType)
{
    int msgID = 0, msgFlg = 0;
    msgFlg = IPC_CREAT | 0666;
    memset (message, 0, sizeof(Message));
    if ((msgID = msgget(key, msgFlg)) < 0)
    {
       qFatal("The message queue wasn't created");
    }
    msgFlg = 0;
    if (msgrcv(msgID, message, sizeof(message->msg), msgType, msgFlg) < 0)
    {
        qDebug() << strerror(errno);
        qFatal ("Couldn't receive a message");
    }
    return 0;
}

void deleteMessageQueue (int key)
{
    int msgID = 0, msgFlg = 0;
    msgFlg = IPC_CREAT | 0666;
    if ((msgID = msgget (key, msgFlg)) < 0)
    {
        qFatal("The message queue wasn't opened");
    }
    if (msgctl(msgID, IPC_RMID, NULL) < 0)
    {
        qFatal("Can't delete the message queue");
    }

}
