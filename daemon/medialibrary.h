#ifndef MEDIALIBRARY_H
#define MEDIALIBRARY_H

#include <QString>
#include <QtSql>
#include <QList>
#include "updateAgent.h"
#include "medialibrarybase.h"

//Class for only one object - our main media library
class MediaLibrary : public MediaLibraryBase
{
    Q_OBJECT
public:
    MediaLibrary(const QString &sdbPath, const QString &connectionName, const QString &pathsFilePath);
    ~MediaLibrary();
    void updateLibrary(); //update library from file mediaLibraryPathsFile (update from directories which mentioned there)
    void updateLibrary(QDir dir); //update library with directory and add it to file mediaLibraryPathsFile and to dirList_
signals:
    void updateAll();
    void updateDir(QDir dir);
private:
    //Thread for update agent
    QThread *thread_;
    //List for directories. Content is equal to mediaLibraryPathsFile
    QList<QDir> dirList_;
    UpdateAgent *agent_;
};

#endif // MEDIALIBRARY_H
