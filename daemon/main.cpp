#include <QCoreApplication>
#include <QDebug>

#include "daemon.h"
#include "messagehandler.h"
#include "../config/our_paths_and_constants.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    qInstallMessageHandler(ourMessageHandler);
    //Check wheather key file exists
    QFile keyFile(keyPath);
    if (keyFile.exists() == 0)
        qFatal("No key file found");
    Daemon d;
    QThread *thread = new QThread;
    QObject::connect(&d,SIGNAL(finished()),&app,SLOT(quit()));
    d.moveToThread(thread);
    QObject::connect(thread,SIGNAL(started()),&d, SLOT(runFetching()));
    QObject::connect(thread,SIGNAL(finished()),&d,SLOT(deleteLater()));
    thread->start();
    return app.exec();
}
