
#include <taglib/fileref.h>
#include <taglib/tstring.h>
#include <taglib/tfile.h>

#include <string>
#include <QFile>
#include <QDebug>
#include <QMediaPlayer>
#include <QMediaMetaData>

#include "libraryaudiofile.h"
TagLib::FileRef::~FileRef() {};

LibraryAudioFile::LibraryAudioFile(QString fullPath)
{

    QFile file(fullPath);
    if (file.exists() == false)
        qWarning() << "file " << fullPath << " wasn't proceeded";
    fullPath_ = fullPath;
    TagLib::FileRef f((fullPath_.toStdString()).c_str());
    TagLib::String title = f.tag()->title();
    TagLib::String artist = f.tag()->artist();
    TagLib::String album = f.tag()->album();
    TagLib::String genre = f.tag()->genre();
    title_ = title.toCString(true);
    artist_ = artist.toCString(true);
    album_ = album.toCString(true);
    length_ = f.audioProperties()->length();
    track_ = f.tag()->track();
    year_ = f.tag()->year();
    bitRate_ = f.audioProperties()->bitrate();

/*
    QMediaPlayer *player = new QMediaPlayer;
    player->setMedia(QUrl::fromLocalFile(fullPath_));
    title_ = player->metaData(QMediaMetaData::Title).toString();
    artist_ = (player->metaData(QMediaMetaData::AlbumArtist)).toString();
    qDebug() << artist_;
    album_ = player->metaData(QMediaMetaData::AlbumTitle).toString();
    genre_ = QString("mainstream");//player->metaData(QMediaMetaData::Genre).toString();
    length_ = player->metaData(QMediaMetaData::Duration).toInt() / 1000;
    track_ = player->metaData(QMediaMetaData::TrackNumber).toInt();
    year_ = player->metaData(QMediaMetaData::Year).toInt();
    bitRate_ = player->metaData(QMediaMetaData::AudioBitRate).toInt();
    delete player;
*/
}
QString LibraryAudioFile::title() const
{
    return title_;
}
QString LibraryAudioFile::artist() const
{
    return artist_;
}
QString LibraryAudioFile::album() const
{
    return album_;
}
QString LibraryAudioFile::path() const
{
    return fullPath_;
}
int LibraryAudioFile::year() const
{
    return year_;
}
int LibraryAudioFile::length() const
{
    return length_;
}
int LibraryAudioFile::bitRate() const
{
    return bitRate_;
}
int LibraryAudioFile::track() const
{
    return track_;
}
