#ifndef DAEMON_H
#define DAEMON_H

#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QFileInfo>

#include "medialibrary.h"
#include "ipcagent.h"


class Daemon: public QObject
{
    Q_OBJECT
public:
    Daemon();
    ~Daemon();
    //Main methods. Receive command and execute it
    int fetchAndExecute();
    //Methods to deal with different commands. Return 1 on success, -1 on error
public slots:
    void runFetching();
signals:
    void finished();
private:
    int play(int32_t specification, const QString &request);
    int add(int32_t specification, const QString &request);
    int addWithoutOverCommand(int32_t specification, const QString &request);
    int next();
    int prev();
    int pause();
    int create();
    int stop();
    int update(int32_t specification, const QString &request);
    int createWithoutOverCommand();
    int load(const QString &playlistName);
    int save(const QString &playlistName);
    int chmode(const QString &newMode);
    int volume(const QString &newVolume);
    int rewind(const QString &whereToRewind);
    int sendInfoAboutFile (QString fullPath);
    int list(int32_t specification, const QString &request);
    int finish();
private:
    QList<QFileInfo> filesInPlaylist_;
    IpcAgent *ipcAgent_;
    QMediaPlaylist *playlist_;
    QMediaPlayer *player_;
    MediaLibrary *mediaLibrary_;
};

#endif // DAEMON_H
