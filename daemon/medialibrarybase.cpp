#include "medialibrarybase.h"
#include "updatemedialibraryfromdirectory.h"
#include "../config/our_paths_and_constants.h"

MediaLibraryBase::MediaLibraryBase(const QString &sdbPath, const QString &connectionName)
{
    //Init database
    sdb_ = QSqlDatabase::addDatabase("QSQLITE", connectionName);
    sdb_.setDatabaseName(sdbPath);
    if (!sdb_.open()) {
        qCritical() << "Couldn't open " << sdbPath;
    }
    //Request new table if not exists
    QSqlQuery a_query(sdb_);
    QString str = "CREATE TABLE IF NOT EXISTS media_library ("
            "path VARCHAR(255) UNIQUE, "
            "track integer, "
            "album VARCHAR(255), "
            "title VARCHAR(255), "
            "year integer, "
            "artist VARCHAR(255), "
            "length integer, "
            "bit_rate integer"
            ");";
    if (!a_query.exec(str)) {
        qCritical() << "Couldn't create table" << a_query.lastError();
    }
}
//First version of request method. Doesn't really care about statement

QString turnIntoRegularExpression(const QStringList &s, int begin, int end) {
    QString result("'%");
    for (int i = begin; i < end; i++)
        result += s[i] + "%";
    result += "'";
    return result;
}

int MediaLibraryBase::request(QList<QFileInfo> &resultList, QString statement)
{
    if (statement.isEmpty() == true)
    {

        QString request = "SELECT * FROM " + QString(tableName) + " ORDER BY artist, year, album, track, title, path ASC";
        QSqlQuery query(sdb_);
        if (!query.exec(request)) {
            qWarning() << "Couldn't make a request";
            return -1;
        }
        QSqlRecord rec = query.record();
        QString path;
        while (query.next()) {
            path = query.value(rec.indexOf("path")).toString();
            resultList.append(QFileInfo(path));
        }
        return resultList.size();
    }
    QTextStream statementStream(&statement);
    QStringList statementList;
    QString temp;
    while (statementStream.atEnd() == false) {
        statementStream >> temp;
        statementList.append(temp);
    }
    QSqlQuery query(sdb_);
    for (int artist = statementList.size(); artist >= 0; artist--)
    {
        QString artistRequest = "SELECT 1 FROM " +
                          QString(tableName) + " WHERE UPPER(artist) LIKE UPPER(" +
                          turnIntoRegularExpression(statementList, 0, artist) + ")";
        for (int album = statementList.size(); album >= artist; album--)
        {
            QString albumAndArtistRequest = " AND UPPER(album) LIKE UPPER(" +
                    turnIntoRegularExpression(statementList, artist, album) + ")" +
                    " AND UPPER(title) LIKE UPPER(" +
                    turnIntoRegularExpression(statementList, album, statementList.size()) + ")";
            QString request = artistRequest + albumAndArtistRequest;
            qDebug() << request;
            if (!query.exec(request)) {
                qWarning() << "Couldn't make a request";
                qWarning() << query.lastError();
                return -1;
            }

            if (query.next() == true)
            {
                qDebug() << "found on request" << request;
                request = "SELECT * FROM " +
                        QString(tableName) + " WHERE UPPER(artist) LIKE UPPER(" +
                        turnIntoRegularExpression(statementList, 0, artist) + ")" + albumAndArtistRequest;
                resultList.clear();
                query.exec(request);
                QSqlRecord rec = query.record();
                QString path;
                while (query.next()) {
                    path = query.value(rec.indexOf("path")).toString();
                    resultList.append(QFileInfo(path));
                }

                return resultList.size();
            }
        }
    }
    return 0;
}

int MediaLibraryBase::list()
{
    QSqlQuery query(sdb_);
    if (!query.exec("SELECT * FROM media_library")) {
        qWarning() << "Couldn't select anyone";
        return -1;
    }
    QSqlRecord rec = query.record();
    int length; //In seconds.
    int track;
    int bitRate;
    int year;
    QString title;
    QString artist;
    QString album;
    QString path;
    while (query.next()) {
        path = query.value(rec.indexOf("path")).toString();
        track = query.value(rec.indexOf("track")).toInt();
        album = query.value(rec.indexOf("album")).toString();
        title = query.value(rec.indexOf("title")).toString();
        year = query.value(rec.indexOf("year")).toInt();
        artist = query.value(rec.indexOf("artist")).toString();
        length = query.value(rec.indexOf("length")).toInt();
        bitRate = query.value(rec.indexOf("bit_rate")).toInt();
        qDebug() << path << track << album << title << year << artist << length << bitRate << "\n";
    }
    return 0;
}
