#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QObject>
#include <QMutex>
#include <QTimer>

#include "../config/our_paths_and_constants.h"
#include "updatemedialibraryfromdirectory.h"
#include "updateAgent.h"

UpdateAgent::UpdateAgent(QList<QDir> &dirList, QSqlDatabase &sdb)
{
    qDebug() << "update agent construction started";
    list_ = dirList;
    sdb_ = sdb;
    updateList_ = list_;
    sem_.release(list_.count());
    qDebug() << "update agent construction ended";
}
UpdateAgent::~UpdateAgent()
{
    //Write all data from list_ to mediaLibraryPathsFile
}
void UpdateAgent::updateLibrary()
{
    qDebug() << "Update library started";
    QDir dir;
    while (true) {
        sem_.acquire(1);
        mutex_.lock();
        qDebug() << "get new directory";
        dir = *(updateList_.begin());
        qDebug() << "update library main process start proceeding" << dir.canonicalPath();
        updateList_.removeFirst();
        mutex_.unlock();
        updateMediaLibraryFromDirectory(sdb_, dir);
        qDebug() << "update library main process proceeding is over";
    }
}
void UpdateAgent::addDir(QDir dir)
{
    qDebug() << "addDir called with dir " << dir.canonicalPath();
    mutex_.lock();
    sem_.release(1);
    list_.push_front(dir);
    updateList_.push_front(dir);
    mutex_.unlock();
}
void UpdateAgent::updateAll()
{
    qDebug() << "Update all called";
    QList<QDir>::iterator it = list_.begin();
    while (it != list_.end()) {
        mutex_.lock();
        sem_.release(1);
        updateList_.push_front(*it);
        mutex_.unlock();
        it++;
    }
}
