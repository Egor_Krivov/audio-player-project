#ifndef LIBRARYAUDIOFILE_H
#define LIBRARYAUDIOFILE_H

#include <QString>
//Class used to get information from tags
class LibraryAudioFile
{
public:
    LibraryAudioFile(QString fullPath);
    int length() const; //In seconds.
    int track() const;
    int bitRate() const;
    int year() const;
    QString title() const;
    QString artist() const;
    QString album() const;
    QString path() const;
private:
    QString fullPath_;
    int track_, bitRate_, length_, year_;
    QString title_, artist_, album_, genre_;
};

#endif // LIBRARYAUDIOFILE_H
