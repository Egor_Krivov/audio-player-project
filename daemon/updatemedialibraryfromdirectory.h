#ifndef UPDATEMEDIALIBRARYFROMDIRECTORY_H
#define UPDATEMEDIALIBRARYFROMDIRECTORY_H

#include "../config/supported_formats.h"

#include <QtSql>
#include <QDir>

int updateMediaLibraryFromDirectory(QSqlDatabase &sdb, QDir &dir); //Return the number of updated files. -1 on error
#endif // UPDATEMEDIALIBRARYFROMFOLDER_H
