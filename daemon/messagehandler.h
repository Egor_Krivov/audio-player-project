#ifndef OUR_MESSAGE_HANDLER_H
#define OUR_MESSAGE_HANDLER_H
#include <QCoreApplication>

void ourMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

#endif // OUR_MESSAGE_HANDLER_H
