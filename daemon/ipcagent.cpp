#include <QFile>
#include <QTextStream>
#include <string.h>
#include <QDebug>

#include "ipcagent.h"
#include "../config/ipc.h"
#include "../config/our_paths_and_constants.h"

IpcAgent::IpcAgent()
{
    QFile keyFile(keyPath);
    if(keyFile.open(QIODevice::ReadOnly) == false) {
        qFatal("Couldn't open keyPath file");
    }
    QTextStream in(&keyFile);
    QString s = in.readLine();
    key_ = s.toInt();
    keyFile.close();
}
IpcAgent::~IpcAgent()
{
    deleteMessageQueue(key_);
    QFile keyFile(keyPath);
    keyFile.remove();
}

int IpcAgent::sendDataToPrint(const QString &s)
{
    qDebug() << "IpcAgent is going to send:" << s;
    QByteArray byteArray = s.toUtf8();
    const char *c = byteArray.constData();
    char string[MSGSZ];
    for (int size = s.size(); size > 0; c += MSGSZ - 1, size -= MSGSZ - 1) {
        strncpy(string, c, MSGSZ - 1);
        string[MSGSZ - 1] = '\0';
        qDebug() << "sending package with " << string;
        if (sendMessage(key_, PRINT, string, COMMAND_FROM_DAEMON_TO_MASTER) != 0) {
            qCritical() << "Couldn't send package";
            return -1;
        }
    }
    return 0;
}
int IpcAgent::receiveCommand(int32_t &command, QString &request)
{
    qDebug() << "IpcAgent is going to receive message";
    if (receiveMessage(key_, &message_, COMMAND_FROM_MASTER_TO_DAEMON) != 0)
        qCritical() << "Couldn't receive message";
    qDebug() << "message received";
    command = message_.msg.command;
    request = message_.msg.buf;
    qDebug() << "received command:" << command << "and request:" << request;
    return 0;
}
int32_t IpcAgent::askMaster()
{
    qDebug() << "IpcAgent is asking master";
    if (sendMessage(key_, GET_ANSWER, NULL, COMMAND_FROM_DAEMON_TO_MASTER) != 0)
        qCritical() << "Couldn't ask master";
    if (receiveMessage(key_, &message_, REPLY_FROM_MASTER_TO_DAEMON) != 0)
        qCritical() << "Couldn't receive message";
    qDebug() << "received reply command: " << message_.msg.command;
    return message_.msg.command;
}
int IpcAgent::sendOverCommand()
{
    if (sendMessage(key_, OVER, 0, COMMAND_FROM_DAEMON_TO_MASTER) != 0) {
        qCritical() << "Couldn't send over command to master";
        return -1;
    }
    qDebug() << "IpcAgent has sent over command";
    return 0;
}
