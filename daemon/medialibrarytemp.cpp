#include <QString>
#include <QFile>

#include "../config/our_paths_and_constants.h"
#include "medialibrarytemp.h"
#include "updatemedialibraryfromdirectory.h"

MediaLibraryTemp::MediaLibraryTemp(const QString &connectionName, const QString &dirPath)
    :MediaLibraryBase(tempDatabasePath, connectionName)
{
    dir_ = QDir(dirPath);
    dir_.makeAbsolute();
}
MediaLibraryTemp::~MediaLibraryTemp() {
    sdb_.close();

    QFile database(tempDatabasePath);
    database.remove();
}
void MediaLibraryTemp::updateLibrary() {
    updateMediaLibraryFromDirectory(sdb_, dir_);
}
