#ifndef MEDIALIBRARYBASE_H
#define MEDIALIBRARYBASE_H

#include <QObject>
#include <QString>
#include <QtSql>
#include <QList>
#include <QMediaContent>
#include <QObject>
//Base class for libraries
class MediaLibraryBase : public QObject
{
    Q_OBJECT
public:
    MediaLibraryBase(const QString &sdbPath, const QString &connectionName);
    virtual void updateLibrary() = 0;
    //That is how we can get information from the library. Returns the number of elements found
    int list();
    int request(QList<QFileInfo> &resultList, QString statement = QString());
protected:
    QSqlDatabase sdb_;
};

#endif
