#include "medialibrary.h"
#include "../config/our_paths_and_constants.h"
#include "updatemedialibraryfromdirectory.h"
#include "updateAgent.h"

MediaLibrary::~MediaLibrary() {
    sdb_.close();
    thread_->quit();
    delete thread_;
}

MediaLibrary::MediaLibrary(const QString &sdbPath,const QString &connectionName, const QString &pathsFilePath)
    :MediaLibraryBase(sdbPath, connectionName)
{
    //Load media library directories to the list
    QFile pathsFile(pathsFilePath);
    if(pathsFile.open(QIODevice::ReadOnly) == false) {
        qCritical() << "opening " + pathsFilePath + " failed";
    }
    QTextStream in(&pathsFile);
    while(!in.atEnd()) {
        QString s = in.readLine();
        QDir d(s);
        if (d.makeAbsolute() == true) {
            dirList_.append(d);
        }
        else
            qWarning() << "Couldn't read directory from the pathsFile";
    }
    pathsFile.close();
    QList<QDir>::iterator it = dirList_.begin();
    //Debug information
    qDebug() << "Media lib paths:";
    while (it != dirList_.end()) {
        qDebug() <<  it->canonicalPath();
        ++it;
    }
    qDebug() << "Media lib paths ended";
    //thread, agent and signals
    thread_ = new QThread;
    agent_ = new UpdateAgent(dirList_, sdb_);
    agent_->moveToThread(thread_);
    connect(this, SIGNAL(updateAll()), agent_, SLOT(updateAll()), Qt::DirectConnection);
    connect(this, SIGNAL(updateDir(QDir)), agent_, SLOT(addDir(QDir)), Qt::DirectConnection);
    connect (thread_, SIGNAL(started()), agent_, SLOT(updateLibrary()));
    //Not sure
    connect (thread_, SIGNAL(finished()), agent_, SLOT(deleteLater()));
    thread_->start();
}

void MediaLibrary::updateLibrary()
{
    qDebug() << "Called update library()";
    emit updateAll();
}
void MediaLibrary::updateLibrary(QDir dir)
{
    qDebug() << "update library in ML called with" << dir.canonicalPath();
    dirList_.push_back(dir);
    //Write to file
    emit updateDir(dir);
}
