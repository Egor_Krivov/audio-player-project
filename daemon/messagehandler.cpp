#include "messagehandler.h"
#include <stdlib.h>
#include <stdio.h>
#include "../config/our_paths_and_constants.h"
#include <QTextStream>
#include <QFile>
#include <QMutex>
QMutex HandlerMutex;
//How do we close file?
void ourMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    HandlerMutex.lock();
    QByteArray localMsg = msg.toLocal8Bit();
    static QString pathToFile(logfilepath);
    static QFile logfile(pathToFile);
    static QTextStream fileOut(&logfile);//stream to file
    static QTextStream consoleOut(stdout);//stream to console
    static bool fileIsntOpened = true;
    if (fileIsntOpened)
    {
        logfile.open(QIODevice::WriteOnly);
        fileIsntOpened = false;
    }
    switch (type) {
        case QtDebugMsg:
            if(ISDEBUG < 1)
                consoleOut<<"Debug: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            fileOut<<"Debug: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            break;
        case QtWarningMsg:
            if(ISDEBUG < 2)
                consoleOut<<"Warning: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            fileOut<<"Warning: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            break;
        case QtCriticalMsg:
            if(ISDEBUG < 3)
                consoleOut<<"Critical: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            fileOut<<"Critical: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            break;
        case QtFatalMsg:
            if(ISDEBUG < 4)
                consoleOut<<"Fatal: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            fileOut<<"Fatal: "<<localMsg.constData()<<" ("<<context.file<<": "<<context.line<<", "<< context.function<<")"<<endl;
            abort();
        }
    HandlerMutex.unlock();
}
