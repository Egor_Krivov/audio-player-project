#ifndef UPDATEAGENT_H
#define UPDATEAGENT_H

#include <QObject>
#include <QMutex>
#include <QtSql>

class UpdateAgent: public QObject
{
    Q_OBJECT
public:
    UpdateAgent(QList<QDir> &dirList, QSqlDatabase &sdb);
    ~UpdateAgent();
public slots:
    void updateLibrary();//endless method which update everything in updateList_
    void addDir(QDir dir);//add dir to updateList_ and list_
    void updateAll();//add all dirs from list_ to updateList_
signals:
    void finished();
private:
    QList<QDir> list_;
    QList<QDir> updateList_;
    QSqlDatabase  sdb_;
    QMutex mutex_;
    QSemaphore sem_;
};

#endif // UPDATEAGENT_H
