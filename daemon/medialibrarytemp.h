#ifndef MEDIALIBRARYTEMP_H
#define MEDIALIBRARYTEMP_H

#include <QDir>
#include <QString>

#include "medialibrarybase.h"
//Exists only one object. Creates temporary database, removes it at the end.
//We need it to add directories.
class MediaLibraryTemp : public MediaLibraryBase
{
    Q_OBJECT
public:
    MediaLibraryTemp(const QString &connectionName, const QString &dirPath);
    ~MediaLibraryTemp();
    void updateLibrary();
private:
    QDir dir_; //Directory with media data
};

#endif // MEDIALIBRARYTemp_H
