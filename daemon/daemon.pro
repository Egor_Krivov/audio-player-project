#-------------------------------------------------
#
# Project created by QtCreator 2014-03-17T00:59:24
#
#-------------------------------------------------

QT       += core
QT       += multimedia
QT       -= gui
QT       += sql

TARGET = daemon
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += ltag

TEMPLATE = app


SOURCES += main.cpp \
    libraryaudiofile.cpp \
    daemon.cpp \
    messagehandler.cpp \
    updatemedialibraryfromdirectory.cpp \
    ../config/ipc.cpp \
    ipcagent.cpp \
    medialibrarybase.cpp \
    updateAgent.cpp \
    medialibrarytemp.cpp \
    medialibrary.cpp

HEADERS += \
    libraryaudiofile.h \
    daemon.h \
    messagehandler.h \
    ../config/our_paths_and_constants.h \
    ../config/supported_formats.h \
    ../config/ipc.h \
    updatemedialibraryfromdirectory.h \
    ipcagent.h \
    medialibrarybase.h \
    updateAgent.h \
    medialibrarytemp.h \
    medialibrary.h

LIBS += -ltag

OTHER_FILES +=
