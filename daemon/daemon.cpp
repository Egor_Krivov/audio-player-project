#include <QString>
#include <QDebug>

#include "libraryaudiofile.h"
#include "daemon.h"
#include "medialibrary.h"
#include "medialibrarytemp.h"
#include "updatemedialibraryfromdirectory.h"
#include "../config/our_paths_and_constants.h"

Daemon::Daemon()
{
    //Multimedia init
    player_ = new QMediaPlayer;
    playlist_ = new QMediaPlaylist;
    player_->setPlaylist(playlist_);
    mediaLibrary_ = new MediaLibrary(databasePath, connectionMain, mediaLibraryPathsFile);
    //ipc init
    ipcAgent_ = new IpcAgent();
    filesInPlaylist_.clear();
}
Daemon::~Daemon()
{
    delete player_;
    delete playlist_;
    delete mediaLibrary_;
    delete ipcAgent_;
}
int Daemon::fetchAndExecute()
{
    int32_t command = 0;
    QString request;
    ipcAgent_->receiveCommand(command, request);
    int32_t specification = command & MASK_FOR_SPECIFICATION;
    int32_t baseCommand = command & MASK_FOR_BASE_COMMAND;
    switch (baseCommand) {
    case PLAY:
        return play(specification, request);
    case ADD:        
        return add(specification, request);
    case STOP:
        return stop();
    case NEXT:
        return next();
    case PREV:
        return prev();
    case PAUSE:
        return pause();
    case CREATE:
        return create();
    case CHMODE:
        return chmode(request);
    case VOL:
        return volume(request);
    case LIST:
        return list(specification, request);
    case REW:
        return rewind(request);
    case UPDATE:
        return update(specification, request);
    case EXIT:
        return finish();
    default:
        ipcAgent_->sendDataToPrint("Unknown command\n");
        ipcAgent_->sendOverCommand();
        qWarning() << "Unknown command received: " << command;
        return 1;
    }
    qFatal("Should never get here");
    return -1;
}
int Daemon::play(int32_t specification, const QString &request)
{
    qDebug() << "Play methed called";
    switch (specification) {
    case NO_DATA:
        if (playlist_->currentIndex() == -1)
            playlist_->setCurrentIndex(0);
        player_->play();
        ipcAgent_->sendOverCommand();
        return 1;
        break;
    case DATA|MEDIA:
    case DATA:
        player_->stop();
        createWithoutOverCommand();
        addWithoutOverCommand(specification, request);
        playlist_->setCurrentIndex(0);
        player_->play();
        ipcAgent_->sendOverCommand();
        return 1;
        break;
    default:
        ipcAgent_->sendDataToPrint("Unknown specification\n");
        ipcAgent_->sendOverCommand();
        return 1;
    }
    return -1;
}
int Daemon::add(int32_t specification, const QString &request)
{
    int r = addWithoutOverCommand(specification, request);
    ipcAgent_->sendOverCommand();
    return r;
}

int Daemon::update(int32_t specification, const QString &request)
{
    switch(specification) {
    case NO_DATA:
        mediaLibrary_->updateAll();
        ipcAgent_->sendOverCommand();
        return 1;
    case DATA:
    {
        QDir dir(request);
        if (dir.exists() == true)
        {
            mediaLibrary_->updateDir(dir);
            ipcAgent_->sendDataToPrint("Update started\n");
        }
        else
        {
            ipcAgent_->sendDataToPrint("No such directory found\n");
        }
        ipcAgent_->sendOverCommand();
        return 1;
    }
    default:
        ipcAgent_->sendDataToPrint("unknown specification\n");
        ipcAgent_->sendOverCommand();
        return 1;
    }
}

int Daemon::addWithoutOverCommand(int32_t specification, const QString &request)
{
    qDebug() << "Daemon add called with request" << request;
    switch (specification) {
    case DATA:
    {
        QFileInfo f(request);
        if (f.exists() == 0)  {
            qDebug() << "No such file or directory found" << request;
            ipcAgent_->sendDataToPrint("No such file or directory found\n");
            return 1;
        }
        else if (f.isFile() == 0) {
            MediaLibraryTemp tempLibrary(connectionTemp, request);
            qDebug() << "temp media lib created";
            tempLibrary.updateLibrary();
            qDebug() << "temporary library update done";
            QList<QFileInfo> newFiles;           
            qDebug() << "found " << tempLibrary.request(newFiles) << "files";
            if (newFiles.isEmpty() == true)
            {
                ipcAgent_->sendDataToPrint("No new files found\n");
            }
            else
            {
                filesInPlaylist_.append(newFiles);
                QList<QFileInfo>::iterator it = newFiles.begin();

                while (it != newFiles.end()) {
                    playlist_->addMedia(QUrl::fromLocalFile(it->canonicalFilePath()));
                    it++;
                }
                ipcAgent_->sendDataToPrint("New directory added\n");
            }
            return 1;
        }
        else {
            qDebug() << "adding file to playlist";            
            playlist_->addMedia(QUrl::fromLocalFile(f.absoluteFilePath()));
            filesInPlaylist_.append(f);
            ipcAgent_->sendDataToPrint("file " + request + " added\n");
            return 1;
        }
    }
    case MEDIA|DATA:
    {
        QList<QFileInfo> newFiles;
        qDebug() << "found " << mediaLibrary_->request(newFiles, request) << "files in media library";
        if (newFiles.isEmpty() == true)
        {
            ipcAgent_->sendDataToPrint("No new files found\n");
        }

        else
        {
            filesInPlaylist_.append(newFiles);
            QList<QFileInfo>::iterator it = newFiles.begin();
            while (it != newFiles.end()) {
                playlist_->addMedia(QUrl::fromLocalFile(it->canonicalFilePath()));
                it++;
            }
            ipcAgent_->sendDataToPrint("New files from media library added\n");
        }
        return 1;
    }
    default:
        qWarning() << "Wrong specification";
        ipcAgent_->sendDataToPrint("unknown command");
        return 1;
    }
    return -1;
}
int Daemon::stop()
{
    player_->stop();
    ipcAgent_->sendOverCommand();
    return 1;
}

int Daemon::next()
{
    playlist_->next();
    ipcAgent_->sendOverCommand();
    return 1;
}
int Daemon::prev()
{
    playlist_->previous();
    ipcAgent_->sendOverCommand();
    return 1;
}
int Daemon::pause()
{
    player_->pause();
    ipcAgent_->sendOverCommand();
    return 1;
}
int Daemon::create()
{
    int r = createWithoutOverCommand();
    ipcAgent_->sendOverCommand();
    return r;
}
int Daemon::createWithoutOverCommand()
{
    qDebug() << "create method called";
    player_->stop();
    playlist_->clear();
    filesInPlaylist_.clear();
    player_->setPlaylist(playlist_);
    ipcAgent_->sendDataToPrint("Current playlist cleared\n");
    return 1;
}


int Daemon::chmode(const QString &newMode)
{
    int mode = newMode.toInt();
    switch (mode) {
    case 0:
        playlist_->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);
        break;
    case 1:
        playlist_->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
        break;
    case 2:
        playlist_->setPlaybackMode(QMediaPlaylist::Sequential);

        break;
    case 3:
        playlist_->setPlaybackMode(QMediaPlaylist::Loop);
        break;
    case 4:
        playlist_->setPlaybackMode(QMediaPlaylist::Random);
        break;
    default:
        ipcAgent_->sendDataToPrint("Need number from 0 to 4 for this command\n");
        ipcAgent_->sendOverCommand();
        return 1;
    }
    ipcAgent_->sendDataToPrint("New mode installed\n");
    ipcAgent_->sendOverCommand();
    return 1;
}

int Daemon::volume(const QString &newVolume)
{
    int volume = newVolume.toInt();
    if (volume >= 0 && volume <= 100) {
        player_->setVolume(volume);
        ipcAgent_->sendDataToPrint("New volume set\n");
        ipcAgent_->sendOverCommand();
    }
    else {
        ipcAgent_->sendDataToPrint("Need number from 0 to 100 for this command\n");
        ipcAgent_->sendOverCommand();
    }
    return 1;
}

int Daemon::rewind(const QString &whereToRewind) {
    //time in seconds
    int time = whereToRewind.toInt();
    time *= 1000; //turn to milliseconds
    if (time >= 0) {
        if(!player_->isSeekable())
        {
            QEventLoop loop;
            QTimer timer;
            timer.setSingleShot(true);
            timer.setInterval(2000);
            loop.connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
            loop.connect(player_, SIGNAL(seekableChanged(bool)), &loop, SLOT(quit()));
            loop.exec();
        }
        player_->setPosition(time);
        ipcAgent_->sendDataToPrint("New time set\n");
        ipcAgent_->sendOverCommand();
    }
    else {
        ipcAgent_->sendDataToPrint("Need time in seconds for this command\n");
        ipcAgent_->sendOverCommand();
    }
    return 1;
}
int Daemon::finish() {
    ipcAgent_->sendDataToPrint("Finishing...\n");
    ipcAgent_->sendOverCommand();
    return 0;
}

int Daemon::sendInfoAboutFile (QString fullPath)
{
    QString dataToPrint;
    LibraryAudioFile currentFile(fullPath);
    dataToPrint = QString::number(currentFile.track()) + "   " + currentFile.title() + "   " + currentFile.album() + "   " + currentFile.artist() + "   " + QString::number(currentFile.length()) + "   " + QString::number(currentFile.bitRate()) + "\n";
    ipcAgent_->sendDataToPrint(dataToPrint);
    return 0;
}

int Daemon::list(int32_t specification, const QString &request)
{
    QList <QFileInfo> ::iterator currentElementInList;
    switch (specification)
    {
        case NO_DATA:
        {
            currentElementInList = filesInPlaylist_.begin();

            while(currentElementInList != filesInPlaylist_.end())
            {
                for (int counter = 0; counter < LIST_SIZE; counter++)
                {
                    if (currentElementInList == filesInPlaylist_.end())
                        break;
                    sendInfoAboutFile((*currentElementInList).filePath());
                    currentElementInList++;
                }
                if (currentElementInList != filesInPlaylist_.end() && ipcAgent_->askMaster() == 0)
                    break;
            }

            ipcAgent_->sendOverCommand();
            break;
        }
        case (MEDIA|DATA):
        {
            QList <QFileInfo> resultList;
            mediaLibrary_->request(resultList, request);
            currentElementInList = resultList.begin();
            while(currentElementInList != resultList.end())
            {
                for (int counter = 0; counter < LIST_SIZE; counter++)
                {
                    if (currentElementInList == resultList.end())
                        break;
                    sendInfoAboutFile((*currentElementInList).filePath());
                    currentElementInList++;
                }
                if (ipcAgent_->askMaster() == 0)
                    break;
            }
            ipcAgent_->sendOverCommand();

            break;
        }
        case PLAYLIST:
        {
            break;
        }
        default:
        {
            ipcAgent_->sendDataToPrint("Wrong specification");
            ipcAgent_->sendOverCommand();
            break;
        }
    }
    return 1;
}
void Daemon::runFetching()
{
    int r = 0;
    while ((r = this->fetchAndExecute()) == 1)
        ;
    emit finished();
}
