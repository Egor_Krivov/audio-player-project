#include <QDebug>
#include <QtSql>
#include <QList>
#include <QString>
#include <iostream>

#include "updatemedialibraryfromdirectory.h"
#include "libraryaudiofile.h"
#include "../config/our_paths_and_constants.h" //for tableName


int rUpdateMediaLibraryFromDirectory(QSqlDatabase &sdb, QDir &dir, const QStringList &filtersList);

int updateMediaLibraryFromDirectory(QSqlDatabase &sdb, QDir &dir)
{
    FILE *f = stdout;
    FILE *e = stderr;
    stderr = 0;
    stdout = 0;

    std::ofstream out();
    std::streambuf *cerrbuf = std::cerr.rdbuf(); //save old buf
    std::cerr.rdbuf(0); //redirect std::cout to out.txt!


    QStringList filtersList;
    fillWithSupportedFormats(filtersList);
    if (dir.makeAbsolute() == false) {
        qWarning() << "Couldn't make absolute path from" << dir.path();
        return 0;
    }
    int r = rUpdateMediaLibraryFromDirectory(sdb, dir, filtersList);
    std::cout.rdbuf(cerrbuf);
    stderr = e;
    stdout = f;

    return r;
}

int rUpdateMediaLibraryFromDirectory(QSqlDatabase &sdb, QDir &dir, const QStringList &filtersList)
{
    int returnCode = 0;
    if (dir.exists() == false)
        return returnCode;
    QFileInfoList dirList = dir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot);
    dir.setNameFilters(filtersList);
    QFileInfoList fileList = dir.entryInfoList(QDir::Files);

    QFileInfoList::iterator fileIt = fileList.begin();
    QSqlQuery query(sdb);
    query.prepare("INSERT OR REPLACE INTO " + QString(tableName) + " (path, track, album, title, year, artist, length, bit_rate)"
                                  "VALUES (:path, :track, :album, :title, :year, :artist, :length, :bit_rate);");
    while (fileIt != fileList.end()) {
        qDebug() << "File:" << fileIt->canonicalFilePath() << " added to media lib";
        LibraryAudioFile audioFile(fileIt->canonicalFilePath());
        query.bindValue(":path", audioFile.path());
        query.bindValue(":track", audioFile.track());
        query.bindValue(":album", audioFile.album());
        query.bindValue(":title", audioFile.title());
        query.bindValue(":year", audioFile.year());        
        query.bindValue(":artist", audioFile.artist());
        query.bindValue(":length", audioFile.length());
        query.bindValue(":bit_rate", audioFile.bitRate());
        if (!query.exec()) {

            qWarning() << "Couldn't instert data" << query.lastError();

            sdb.close();
            sdb.open();
            query.prepare("INSERT OR REPLACE INTO " + QString(tableName) + " (path, track, album, title, year, artist, length, bit_rate)"
                                          "VALUES (:path, :track, :album, :title, :year, :artist, :length, :bit_rate);");
            query.bindValue(":path", audioFile.path());
            query.bindValue(":track", audioFile.track());
            query.bindValue(":album", audioFile.album());
            query.bindValue(":title", audioFile.title());
            query.bindValue(":year", audioFile.year());
            query.bindValue(":artist", audioFile.artist());
            query.bindValue(":length", audioFile.length());
            query.bindValue(":bit_rate", audioFile.bitRate());
            if (!query.exec()) {

                qWarning() << "Couldn't instert data" << query.lastError();
            }
            returnCode++;

        }
        else
            returnCode++;
        ++fileIt;
    }

    QFileInfoList::iterator dirIt = dirList.begin();
    while (dirIt != dirList.end()) {
        QDir dir(dirIt->canonicalFilePath());
        int r = rUpdateMediaLibraryFromDirectory(sdb, dir, filtersList);
        if (r == -1)
            return -1;
        else
            returnCode += r;
        ++dirIt;
    }
    return returnCode;
}
