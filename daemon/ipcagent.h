#ifndef IPCAGENT_H
#define IPCAGENT_H

#include <QString>
#include "../config/ipc.h"

class IpcAgent
{
public:
    IpcAgent();
    ~IpcAgent();
    int sendDataToPrint(const QString &s); //returns 0 on success
    int receiveCommand(int32_t &command, QString &request);  //return 0 on success
    //returns reply code. -1 on error
    int32_t askMaster();
    int sendOverCommand();
private:
    int key_;
    Message message_;
};

#endif // IPCAGENT_H
