#include <QDebug>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <iostream>

#include "../config/our_paths_and_constants.h"
#include "create_daemon.h"
#include "../config/ipc.h"

int main(int argc, char *argv[])
{
    int com = 0;
    int key = 0;
    if (argc == 1)
        qFatal("Please enter your command");
    QString mesBuf, commBuf(argv[1]);
    QTextStream Qcout(stdout);
    QTextStream Qcin(stdin);
//..Recognise Command from string to int
    if ((com = recogniseBaseCommand(commBuf)) < 0)
    {
        qFatal ("The command is wrong. Please, try again.");
    }
    for (int i = 2; i < argc; i++) {
        if (argv[i][0] == '-') {
            if (recogniseSpecCommand(argv[i]) < 0)
                qFatal ("The command is wrong. Please, try again.");           
            else            
                com |= recogniseSpecCommand(argv[i]);
        }
        else {
            com |= DATA;
            if (i == 2)
                mesBuf.append(argv[i]);
            else
                mesBuf.append(QString(" ") + argv[i]);
        }
    }

    //qDebug() << "Command =" << com;
    //qDebug() << "Request =" << mesBuf;
//..Checking Daemon on existing
    if ((key = DaemonExists()) == -2)
    {
        //qDebug() << "Daemon doesn't exist";
        if (DaemonCreate(&key) < 0)
        {
            qFatal ("Couldn't create a daemon");
        }
    }
    else if (key < -1)
    {
        qFatal ("Error with reading from KeyFile");
    }
    else
    {
        //qDebug() << "The key is: " << key;
    }
//Send message to daemon       
    sendMessage(key, com, mesBuf, COMMAND_FROM_MASTER_TO_DAEMON);

//Receive answer
    Message message;
    do
    {
        receiveMessage(key, &message, COMMAND_FROM_DAEMON_TO_MASTER);
        if (message.msg.command == PRINT)
            Qcout << message.msg.buf;
        if (message.msg.command == GET_ANSWER)
        {
            Qcout << "Would you like to list next 20 compositions?" << endl;
            Qcin >> mesBuf;
            if ((mesBuf == "y") || (mesBuf == "Y"))
                com = 1;
            else if ((mesBuf == "n") || (mesBuf == "N"))
                com = 0;
            else
                Qcout << "Wrong answer";
            sendMessage(key, com, "0", REPLY_FROM_MASTER_TO_DAEMON);
        }
    } while (message.msg.command != OVER);

    if ((com & MASK_FOR_BASE_COMMAND) == PLAY) {
        qDebug() << "Start playing"; //Important slow down next sending

        sendMessage(key, PLAY, 0, COMMAND_FROM_MASTER_TO_DAEMON);
        do
        {
            receiveMessage(key, &message, COMMAND_FROM_DAEMON_TO_MASTER);
            if (message.msg.command == PRINT)
                Qcout << message.msg.buf;
            if (message.msg.command == GET_ANSWER)
            {
                Qcout << "Would you like to list next 20 compositions?" << endl;
                Qcin >> mesBuf;
                if ((mesBuf == "y") || (mesBuf == "Y"))
                    com = 1;
                else if ((mesBuf == "n") || (mesBuf == "N"))
                    com = 0;
                else
                    Qcout << "Wrong answer\n";
                sendMessage(key, com, "0", REPLY_FROM_MASTER_TO_DAEMON);
            }
        } while (message.msg.command != OVER);
    }
    //Qcout << "correct ending" << endl;
    return 0;
}
