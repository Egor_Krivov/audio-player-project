#-------------------------------------------------
#
# Project created by QtCreator 2014-03-23T13:19:36
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = master
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    create_daemon.cpp \
    commands.cpp \
    ../config/ipc.cpp

HEADERS += \
    ../config/structures_and_constants.h \
    ../config/our_paths.h \
    create_daemon.h \
    ../config/key.h \
    ../config/our_paths_and_constants.h \
    ../config/ipc.h
