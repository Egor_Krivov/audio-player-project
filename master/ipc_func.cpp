#include <iostream>
#include <sys/msg.h>
#include "../config/structures_and_constants.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

int sendMessage (key_t key, int iCom, char *cBufMes, int iMsgType)
{
    int iMsgID = 0, iMsgFlg = 0, iBufLength = 0;
    iMsgFlg = IPC_CREAT | 0666;
    Message sMesBuf;
    memset (&sMesBuf, 0, sizeof(Message));

//..Opening or creating message queue
    std::cout << "Creating a message queue with key: " << key << " and flag: " << iMsgFlg << std::endl;
    if ((iMsgID = msgget(key, iMsgFlg)) < 0)
    {
        std::cout << "The message queue wasn't created: " << strerror(errno) << std::endl;
        exit(1);
    }
    else
    {
        std::cout << "Created the message queue with ID:" << iMsgID << std::endl;
    }

//..Saving data to struct message
    sMesBuf.mtype = iMsgType;
    sMesBuf.command = iCom;
    std::cout << "The command: " << sMesBuf.command << " was received" << std::endl;
    strcpy (sMesBuf.msg, cBufMes);
    std::cout << "The message: " << sMesBuf.msg << " was received" << std::endl;

//..Send message
    iBufLength = strlen(sMesBuf.msg) + sizeof(int) + 1;
    if (msgsnd(iMsgID, &sMesBuf, iBufLength,iMsgFlg) < 0)
    {
        perror ("Couldn't send a message");
        exit(1);
    }
    std::cout << "The command: " << std::endl << sMesBuf.command << " was sent" << std::endl;
    std::cout << "The message: " << std::endl << sMesBuf.msg << std::endl << "was sent" << std::endl;
    //msgctl(iMsgID, IPC_RMID, NULL);
    return 0;
}

int receiveMessage (key_t key, Message *msg, int iMsgType)
{
    int iMsgID = 0, iMsgFlg = 0;
    iMsgFlg = IPC_CREAT | 0666;
    memset (msg, 0, sizeof(Message));
    std::cout << "Creating a message queue with key: " << key << " and flag: " << iMsgFlg << std::endl;
    if ((iMsgID = msgget(key, iMsgFlg)) < 0)
    {
        perror ("The message queue wasn't created");
        exit(1);
    }
    else
    {
        std::cout << "Created the message queue with ID:" << iMsgID << std::endl;
    }
    if (msgrcv(iMsgID, msg, MSGSZ, iMsgType, iMsgFlg) < 0)
    {
        perror ("Couldn't receive a message");
        exit(1);
    }
    std::cout << "The command: " << std::endl << msg->command << " was received" << std::endl;
    std::cout << "The message: " << std::endl << msg->msg << std::endl << "was received." << std::endl;
    return 0;
}
