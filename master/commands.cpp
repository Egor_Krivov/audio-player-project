#include <QString>
#include <sys/types.h>
#include "../config/ipc.h"

int recogniseBaseCommand (QString command)
{
    if (command == "play") return PLAY;
    if (command == "add") return ADD;
    if (command == "del") return DELETE;
    if (command == "next") return NEXT;
    if (command == "prev") return PREV;
    if (command == "pause") return PAUSE;
    if (command == "stop") return STOP;
    if (command == "create") return CREATE;
    if (command == "load") return LOAD;
    if (command == "save") return SAVE;
    if (command == "list") return LIST;
    if (command == "chmode") return CHMODE;
    if (command == "vol") return VOL;
    if (command == "exit") return EXIT;
    if (command == "rew") return REW;
    if (command == "update") return UPDATE;
    return -1;
}

int recogniseSpecCommand (QString command)
{
    if (command == "-m") return MEDIA;
    if (command == "-p") return PLAYLIST;
    return -1;
}
