#include <QString>
#include "../config/our_paths_and_constants.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <QDebug>
#include <QTextStream>
#include <QFile>

int generateKey ()
{
    int key = 0;
    //Magic numbers
    if ((key = (ftok(keyPath, PROJ) % 4096)) == 0)
        key = generateKey();
    return key;
}

int DaemonExists ()
{  
    int key = 0;

//..Open file with key
    QFile keyFile(keyPath);
    if (!keyFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        qFatal("Error with KeyFile");
    }

//..Reading key
    QTextStream in(&keyFile);
    QString keyBuf = in.readLine();
    keyFile.close();
    if (!keyBuf.isNull())
    {
        key = keyBuf.toInt();
        return key;
    }
    else if (keyBuf.isNull())
        return -2;
    return -1;
}

int DaemonCreate (int *key)
{
    QString keyBuf = "0";

    *key = generateKey();
    keyBuf = QString::number(*key);
    //qDebug() << "New key is " << keyBuf;

//..Open file with key
    QFile keyFile(keyPath);
    if (!keyFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        qFatal("Error with KeyFile");
    }


//..Writing key into keyFile
    QTextStream out(&keyFile);
    out << keyBuf;
    keyFile.close();

//..Creating daemon
    //qDebug() << "Starting create daemon";
    if (fork() == 0)
    {
        //qDebug() << "Start";
        execl(daemonPath, daemonPath, NULL);
    }
    return 0;
}


