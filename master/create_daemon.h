#ifndef CREATE_DAEMON_H
#define CREATE_DAEMON_H
#include <QFile>

int DaemonExists ();
int DaemonCreate (int *key);

#endif // CREATE_DAEMON_H
