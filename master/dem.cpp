#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../config/our_paths.h"
#include "../config/commands.h"
#include "../config/structures_and_constants.h"

using namespace std;

int main (int argv, char *argc[])
{
	
	int iMsgID = 0, iErr = 0, iMsgFlg = 0, iMsgType = 1;
	//struct msqid_ds qstatus;
	key_t key = 6760;
	iMsgFlg = IPC_CREAT | 0666;
	MESSAGE sMesBuf;
	memset (&sMesBuf, 0, sizeof(MESSAGE));
	cout << "Creating a message queue with key: " << key << " and flag: " << iMsgFlg << endl;
	if ((iMsgID = msgget(key, iMsgFlg)) < 0)
	{
		perror ("The message queue wasn't created");
		exit(1);
	}
	else 
	{
		cout << "Created the message queue with ID:" << iMsgID << endl;
	}
	while (true)
	{
	if (msgrcv(iMsgID, &sMesBuf, MSGSZ, iMsgType, iMsgFlg) < 0)
	{
		perror ("Couldn't receive a message");
		exit(1);
	}
	cout << "The command: " << endl << sMesBuf.command << " was received" << endl;
	cout << "The message: " << endl << sMesBuf.msg << endl << "was received." << endl;
}
	
	
		/*
	printf("Real user id of the queue creator: %d\n",qstatus.msg_perm.cuid);
    printf("Real group id of the queue creator: %d\n",qstatus.msg_perm.cgid);

    printf("Effective user id of the queue creator: %d\n",qstatus.msg_perm.uid);
    printf("Effective group id of the queue creator: %d\n",qstatus.msg_perm.gid);
    printf("Permissions: %d\n",qstatus.msg_perm.mode);
    printf("Message queue id: %d\n", msgID);
    printf("%d message(s) on queue\n",qstatus.msg_qnum);
    printf("Last message sent by process :%3d at %s \n",qstatus.msg_lspid,ctime(& (qstatus.msg_stime)));

    printf("Last message received by process :%3d at %s \n",qstatus.msg_lrpid,ctime(& (qstatus.msg_rtime)));
    printf("Current number of bytes on queue %d\n",qstatus.msg_cbytes);
    printf("Maximum number of bytes allowed on the queue%d\n",qstatus.msg_qbytes);*/
	
	//if (msgctl(msgID, IPC_RMID, NULL) < 0)
	//	cout << "ERROR"; 
	
	
	return 0;
}
