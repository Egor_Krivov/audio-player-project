#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include "../config/our_paths.h"
#include "../config/commands.h"

using namespace std;

extern int errno;


int commands_asm (char *command)
{
	if (strcmp(command, "play") == 0) return PLAY;
	if (strcmp(command, "add") == 0) return ADD;
	if (strcmp(command, "delete") == 0) return DELETE;
	if (strcmp(command, "next") == 0) return NEXT;
	if (strcmp(command, "prev") == 0) return PREV;
	if (strcmp(command, "pause") == 0) return PAUSE;
	if (strcmp(command, "create") == 0) return CREATE;
	if (strcmp(command, "load") == 0) return LOAD;
	if (strcmp(command, "save") == 0) return SAVE;
	if (strcmp(command, "list") == 0) return LIST;
	if (strcmp(command, "chmode") == 0) return CHMODE;
	if (strcmp(command, "vol") == 0) return VOL;	
	if (strcmp(command, "exit") == 0) return EXIT;	
	if (strcmp(command, "rew") == 0) return REW;
	//if (strcmp(
	return -1;
}

struct message 
{
	long mtype;
	char msg[30];
} mes;

int main (int argv, char *argc[])
{
	/*
	int msgID, err;
	//struct msqid_ds qstatus;
	key_t key = 0x5498;
	msgID = msgget(key, IPC_CREAT);
	cout << msgID << endl;
	mes.mtype = 1;
	mes.msg[0] = 'a';
	mes.msg[1] = '\0';
	err = msgsnd (msgID, &mes, sizeof(mes), 0);
	cout << err << endl;
	
	printf("Real user id of the queue creator: %d\n",qstatus.msg_perm.cuid);
    printf("Real group id of the queue creator: %d\n",qstatus.msg_perm.cgid);

    printf("Effective user id of the queue creator: %d\n",qstatus.msg_perm.uid);
    printf("Effective group id of the queue creator: %d\n",qstatus.msg_perm.gid);
    printf("Permissions: %d\n",qstatus.msg_perm.mode);
    printf("Message queue id: %d\n", msgID);
    printf("%d message(s) on queue\n",qstatus.msg_qnum);
    printf("Last message sent by process :%3d at %s \n",qstatus.msg_lspid,ctime(& (qstatus.msg_stime)));

    printf("Last message received by process :%3d at %s \n",qstatus.msg_lrpid,ctime(& (qstatus.msg_rtime)));
    printf("Current number of bytes on queue %d\n",qstatus.msg_cbytes);
    printf("Maximum number of bytes allowed on the queue%d\n",qstatus.msg_qbytes);
	
	//if (msgctl(msgID, IPC_RMID, NULL) < 0)
		//cout << "ERROR"; 
	while(1);
	
	*/
	
	
	
	
	
	int pipe_out_fd = 0;
	int pipe_in_fd = 0;
	int iNumOfCmd = 0;
	int result = 0;
	cout << "Making fifos" << endl; 
	(void)umask(0); 
	mkfifo(pipepath_out, S_IFIFO | 0666);
	mkfifo(pipepath_in, S_IFIFO | 0666);
	cout << "Making fork" << endl;
//	if (strcmp(strerror(errno), "EEXIST") == 0)
//	{

//	}

//DEMON:
DEMON_WORK:
	
	while (1)
	{
		result = 0;
		cout << "Demon is opening fifo to get command from Master" << endl;
		if (-1 == (pipe_in_fd = open (pipepath_out, O_RDONLY)))
		{
			cout << "Demon is unable to open pipe file to read from" << endl;
			return -1;
		}
		if (sizeof(int) != read (pipe_in_fd,&result,sizeof(int)))
		{
			cout << "Demon is unable to read from pipe." << endl;
			continue;
		}
		close(pipe_in_fd);
		cout << "Demon got command:" << result << endl;
		if (result == EXIT)
			break;
		iNumOfCmd++;
		cout << "Demon is sending answer..." << endl;
		if (-1 == (pipe_out_fd = open (pipepath_in, O_WRONLY)))
		{
			cout << "Demon is unable to open pipe file to write to" << endl;
			return -1;
		}
		cout << "Trying to send to Master data " << iNumOfCmd << endl;
		if (sizeof(int) != write (pipe_out_fd,&iNumOfCmd,sizeof(int)))
		{
			cout << "Demon is unable to write to pipe." << endl;
			continue;
		}
		close(pipe_out_fd);
	}
	goto FINALIZE;



	
FINALIZE://*/

	return 0;
}
