#ifndef COMMANDS_H
#define COMMANDS_H

enum base_commands {PLAY = 0x10, ADD = 0x20, DELETE = 0x30, NEXT = 0x40, PREV = 0x50, 
					  PAUSE = 0x60, CREATE = 0x70, LOAD = 0x80, SAVE = 0x90, LIST = 0xA0, 
					  CHMODE = 0xB0, VOL = 0xC0, EXIT = 0xD0, REW = 0xE0};
enum spec_commands {DATA = 0x1, MEDIA = 0x2, PLAYLIST = 0x4};

int recogniseCommand (char *command);

#endif
