#ifndef _STRUCTURES_AND_CONSTANTS_
#define _STRUCTURES_AND_CONSTANTS_

#define MSGSZ 0x1000
extern int errno;
struct Message
{
	long mtype;
	int command;
	char msg[MSGSZ];
};

#endif
