#include <QCoreApplication>
#include <QtSql>
#include <QDebug>
#include <QString>

int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);
    /*QSqlDatabase dbase = QSqlDatabase::addDatabase("QSQLITE");
    dbase.setDatabaseName("my_db.sqlite");
    if (!dbase.open()) {
        qDebug() << "Couldn't open";
        return -1;
    }

    QSqlQuery a_query;
    // DDL query
    QString str = "CREATE TABLE IF NOT EXISTS my_table ("
            "number integer PRIMARY KEY, "
            "name VARCHAR(255) UNIQUE,"
            "age integer"
            ");";
    bool b = a_query.exec(str);
    if (!b) {
        qDebug() << "Couldn't create table" << a_query.lastError();
    }

    // DML
    a_query.prepare("INSERT OR REPLACE INTO my_table (name, age)"
                                  "VALUES (:name, :age);");
    a_query.bindValue(":name", "tom");
    a_query.bindValue(":age", "17");
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "Couldn't instert data";
    }

    if (!a_query.exec("SELECT * FROM my_table")) {
        qDebug() << "Couldn't select anyone";
        return -2;
    }
    QSqlRecord rec = a_query.record();
    int number = 0,
            age = 0;
    QString name = "";
    qDebug() << "not dead";
    while (a_query.next()) {

        //number = a_query.value(rec.indexOf("number")).toInt();
        age = a_query.value(rec.indexOf("age")).toInt();
        name = a_query.value(rec.indexOf("name")).toString();

        qDebug() << "number is " << number
                 << ". age is " << age
                 << ". name" << name;
    }
    */
    QSqlDatabase dbase = QSqlDatabase::addDatabase("QSQLITE");
        dbase.setDatabaseName("my_db.sqlite");
        if (!dbase.open()) {
            qDebug() << "Couldn't open";
            return -1;
        }

        QSqlQuery a_query;
        // DDL query
        QString str = "CREATE TABLE IF NOT EXISTS my_table ("
                "name VARCHAR(255) UNIQUE, "
                "age integer"
                ");";
        bool b = a_query.exec(str);
        if (!b) {
            qDebug() << "Couldn't create table";
        }

        // DML
        a_query.prepare("INSERT OR REPLACE INTO my_table (name, age)"
                                      "VALUES (:name, :age);");

        a_query.bindValue(":name", "tom");
        a_query.bindValue(":age", "17");
        b = a_query.exec();
        if (!b) {
            qDebug() << "Couldn't instert data";
        }
        //.....
        if (!a_query.exec("SELECT * FROM my_table")) {
            qDebug() << "Couldn't select anyone";
            return -2;
        }
        QSqlRecord rec = a_query.record();
        int age = 0;
        QString name = "";

        while (a_query.next()) {
            age = a_query.value(rec.indexOf("age")).toInt();
            name = a_query.value(rec.indexOf("name")).toString();

                     qDebug() << ". age is " << age
                     << ". name" << name;
        }
        return 0;
}
