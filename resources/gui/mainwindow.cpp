#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->label->setText("oops");
    QObject::connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(MyEventHandler()));
    QObject::connect(this, SIGNAL(MySignal(QString)), ui->label, SLOT(setText(QString)));
}

void MainWindow::MyEventHandler()
{
    ui->label->setText("ooops");
    emit MySignal (ui->label->text());
}

MainWindow::~MainWindow()
{
    delete ui;
}
