#include <QCoreApplication>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <iostream>
#include <QString>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    QMediaPlaylist *playlist = new QMediaPlaylist;
    playlist->addMedia(QUrl::fromLocalFile("/home/egor/1.mp3"));
    playlist->addMedia(QUrl::fromLocalFile("/home/egor/2.mp3"));
    playlist->addMedia(QUrl::fromLocalFile("/home/egor/1.flac"));


    QMediaPlayer *player = new QMediaPlayer;
    player->setPlaylist(playlist);
    playlist->setCurrentIndex(0);
    player->setVolume(20);
    player->play();


    int n;
    std::cin >> n;
    while (n != 0) {
        switch (n) {
        case 1 :
            playlist->next();
            break;
        case 2 :
            playlist->previous();
            break;
        default:
            std::cout << "use 0,1 or 2";
        }
        std::cout << "current index " << playlist->currentIndex() << std::endl;
        if (playlist->currentIndex() == -1) {
            playlist->setCurrentIndex(0);
            player->play();
        }

        std::cin >> n;
    }
    delete player;
    delete playlist;

    return a.exec();
}
